import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) {

        int[] arr = {8,7,2,5,3,1};
        int[] arr1 = {2,5,5,5,5,6,6,8,9,9};
        findAPairWithSum10(arr);
        findHowManyTimesAnIntegerOccursInGivenArray(arr1,8);
        System.out.println(findIfAnElementExistsInAListLinear(arr,2));


    }

    /**
     * Types the pair of numbers in a given array whose sum equals 10
     * @param input
     */
    public static void findAPairWithSum10 ( int[] input){
        IntStream.range(0,  input.length)
                .forEach(i -> IntStream.range(0,  input.length)
                        .filter(j -> i < j && input[i] + input[j] == 10)
                        .forEach(j -> System.out.println("Pair found at index " + i + " and " + j + " (" + input[i] + "+" + input[j] + ")"))
                );
    }

    /**
     * Types how many times a given number appears in a given list.
      * @param input
     * @param number
     */
    public static void findHowManyTimesAnIntegerOccursInGivenArray (int[] input, int number){
        long count = IntStream.of(input).filter(i -> i==number).count();
        System.out.println("Element "+number+" occurs "+count+" times");
    }

    /**
     * Returns the index of a given number in a given list(Linear solution).
     * If the number isn't on the list returns -1
     * @param input
     * @param inputNumber
     * @return
     */
    public static int findIfAnElementExistsInAListLinear (int[] input, int inputNumber){
         return IntStream.range(0, input.length)
                .filter(i -> inputNumber == input[i])
                .findFirst()
                .orElse(-1);
    }
}
